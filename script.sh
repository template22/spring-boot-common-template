#!/bin/bash

if [[ $# -ne 7 ]]; then
    echo "Usage ./script.sh {project_name} {maven_group_id} {maven_version} {maven_parent_version} {maven_parent_repository} {ci_project} {ci_file}"
    exit 2
fi

project_name=$1
maven_group_id=$2
maven_version=$3
maven_parent_version=$4
maven_parent_repository=$5
ci_project=$6
ci_file=$7

echo $maven_group_id
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#MAVEN_GROUP_ID~$maven_group_id~g" {} +
echo $maven_version
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#MAVEN_VERSION~$maven_version~g" {} +
echo $maven_parent_version
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#MAVEN_PARENT_VERSION~$maven_parent_version~g" {} +
echo $maven_parent_repository
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#MAVEN_PARENT_REPOSITORY_URL~$maven_parent_repository~g" {} +
echo $ci_project
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#CI_PROJECT~$ci_project~g" {} +
echo $ci_file
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#CI_FILE~$ci_file~g" {} +