package com.gbournac.question.common.services.logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;

/**
 * The log formater
 * 
 * @author Guénaël Bournac
 *
 */
public class LogFormater {

	private static String NO_USER_ID = "0";

	/**
	 * Info log method in case of no user connected.
	 *
	 * @param logger      Logger.
	 * @param sMethodName Method name.
	 * @param msg         Message.
	 * @param extraArgs   Extra arguments.
	 */
	public static void info(final Logger logger, final String sMethodName, final String msg,
			final Object... extraArgs) {
		if (logger.isInfoEnabled()) {
			logger.info("[{}] - [{}] : " + msg, buildNewExtraArgs(sMethodName, NO_USER_ID, extraArgs));
		}
	}

	/**
	 * Debug log method in case of no user connected.
	 *
	 * @param logger      Logger.
	 * @param sMethodName Method name.
	 * @param msg         Message.
	 * @param extraArgs   Extra arguments.
	 */
	public static void debug(final Logger logger, final String sMethodName, final String msg,
			final Object... extraArgs) {

		if (logger.isDebugEnabled()) {
			logger.debug("[{}] - [{}] : " + msg, buildNewExtraArgs(sMethodName, NO_USER_ID, extraArgs));
		}
	}

	/**
	 * Warn log method in case of no user connected.
	 *
	 * @param logger      Logger.
	 * @param sMethodName Method name.
	 * @param msg         Message.
	 * @param extraArgs   Extra arguments.
	 */
	public static void warn(final Logger logger, final String sMethodName, final String msg,
			final Object... extraArgs) {

		logger.warn("[{}] - [{}] : " + msg, buildNewExtraArgs(sMethodName, NO_USER_ID, extraArgs));
	}

	/**
	 * Error log method in case of no user connected.
	 *
	 * @param logger      Logger.
	 * @param sMethodName Method name.
	 * @param msg         Message.
	 * @param extraArgs   Extra arguments.
	 */
	public static void error(final Logger logger, final String sMethodName, final String msg,
			final Object... extraArgs) {

		logger.error("[{}] - [{}] : " + msg, buildNewExtraArgs(sMethodName, NO_USER_ID, extraArgs));
	}

	/**
	 * Method to transform extra args.
	 *
	 * @param sMethodName Method name.
	 * @param userId      Connected user.
	 * @param extraArgs   Extra args
	 * @return List of extra args.
	 */
	private static Object[] buildNewExtraArgs(final String sMethodName, final String userId,
			final Object... extraArgs) {

		List<Object> argList = new ArrayList<>();
		argList.add(userId);
		argList.add(sMethodName);
		argList.addAll(Arrays.asList(extraArgs));

		return argList.toArray();
	}

}
